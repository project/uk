# LANGUAGE translation of Drupal (modules/queue.module)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from file: queue.module,v 1.116 2004/09/16 07:17:55 dries
#
msgid ""
msgstr ""
"Project-Id-Version: Maidan\n"
"POT-Creation-Date: 2005-01-15 12:31+0000\n"
"PO-Revision-Date: 2005-08-15 15:45-0500\n"
"Last-Translator: \n"
"Language-Team: UKRAINIAN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Ukrainian\n"
"X-Poedit-Country: UKRAINE\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: modules/queue.module:15
msgid "Allows content to be moderated by the community."
msgstr "Дозволяє громаді модерувати вміст."

#: modules/queue.module:17
msgid "<p>The queue provides a way for your users to vote on submitted content. This is called <strong>moderation</strong>. Users can moderate a post up (give it a point), or down (subtract a point). The settings below give you control over how many points are required for the status of a post to be automatically changed. See individual items for details.</p>"
msgstr "<p>Черга (queue) дозволяє користувачам голосувати за поданий вміст. Цей процес зветься <strong>модерація</strong>. Користувачі можуть модерувати (просувати) допис доверху (додаючи оцінки), або донизу (віднімаючи оцінки). Нижченаведені налаштування дозволяють контролювати кількість оцінок для автоматичної зміни статусу допису. Перегляньте інформацію про окремі елементи для з'ясування деталей.</p>"

#: modules/queue.module:25
msgid "Post threshold"
msgstr "Поріг оприлюднення"

#: modules/queue.module:25
msgid "When a post gets this number of moderation points, it is <strong>promoted to the front page</strong> automatically."
msgstr "Коли допис отримує дану кількість модераційних оцінок, він автоматично <strong>розміщується на головній сторінці.</strong>"

#: modules/queue.module:26
msgid "Dump threshold"
msgstr "Поріг прибирання"

#: modules/queue.module:26
msgid "When a post drops below this number of points, its status is changed to <strong>unpublished</strong>."
msgstr "Коли оцінки допису опускаються нижче цього рівня, його статус змінюється на <strong>неоприлюднений</strong>."

#: modules/queue.module:27
msgid "Expiration threshold"
msgstr "Поріг застарівання"

#: modules/queue.module:27
msgid "When a post gets this number of points, its status is changed to <strong>unpublished</strong>."
msgstr "Коли оцінки допису опускаються нижче цього рівня, його статус змінюється на <strong>неоприлюднений</strong>."

#: modules/queue.module:28
msgid "Show comments"
msgstr "Показати коментарі"

#: modules/queue.module:28
msgid "Tick the box to show comments below the moderation form."
msgstr "Щоб показати коментарі під формою модерації, поставте позначку тут."

#: modules/queue.module:47
msgid "submission queue"
msgstr "черга подання"

#: modules/queue.module:81
msgid "Moderation: approved %title."
msgstr "Модерація: %title схвалено."

#: modules/queue.module:82
msgid "The post is promoted."
msgstr "Допис розміщено на першій сторінці."

#: modules/queue.module:87
msgid "Moderation: declined %title (rollback)."
msgstr "Модерація: не схвалено %title (повернуто назад)."

#: modules/queue.module:88
msgid "The post has been declined and the previous version has been restored."
msgstr "Допис не схвалено, було поновлено попередню версію."

#: modules/queue.module:94
msgid "Moderation: declined %title."
msgstr "Модерація: %title не схвалено."

#: modules/queue.module:95
msgid "The post has been declined."
msgstr "Допис не схвалено."

#: modules/queue.module:101
msgid "Moderation: expired %title (rollback)."
msgstr "Модерація: застарілий %title (повернуто назад)."

#: modules/queue.module:102
msgid "The post has expired and the previous version has been restored."
msgstr "Допис застарів, було поновлено попередню версію."

#: modules/queue.module:108
msgid "Moderation: expired %title."
msgstr "Модерація: %title застарів."

#: modules/queue.module:109
msgid "The post has expired."
msgstr "Допис застарів."

#: modules/queue.module:124
msgid "Score"
msgstr "Оцінки"

#: modules/queue.module:145
msgid "No posts available in queue."
msgstr "Жодних дописів у черзі."

#: modules/queue.module:152
msgid "Submission queue"
msgstr "Черга подання"

#: modules/queue.module:167
msgid "neutral (+0)"
msgstr "байдуже (+0)"

#: modules/queue.module:167
msgid "post it (+1)"
msgstr "оприлюднити (+1)"

#: modules/queue.module:167
msgid "dump it (-1)"
msgstr "прибрати (-1)"

#: modules/queue.module:179
msgid "Your vote has been recorded."
msgstr "Ваш голос було зараховано."

#: modules/queue.module:183
msgid "When new content is submitted, it goes into the submission queue.  Registered users with the appropriate permission can access this queue and vote whether they think the content should be approved or not.  When enough people vote to approve the content, it is displayed on the front page.  On the other hand, if enough people vote to drop it, the content will disappear."
msgstr "Коли подається новий вміст, він потрапляє у чергу подання. Зареєстровані користувачі з певними повноваженнями мають доступ до черги і можуть голосувати, таким чином схвалюючи чи не схвалюючи оприлюднення на першій сторінці. Коли достатня кількість людей проголосувала схвально, допис потрапляє на першу сторінку. З іншого боку, коли достатня кількість людей проголосувала, щоб прибрати допис, він зникає. "

#: modules/queue.module:186
msgid "Your vote"
msgstr "Ваш голос"

#: modules/queue.module:195
msgid "Moderate"
msgstr "Модерувати"

#: modules/queue.module:226
#: ;245
msgid "Moderation results"
msgstr "Результати модерації"

#: modules/queue.module:242
msgid "%user voted %vote"
msgstr "%user проголосував(ла) %vote"

#: modules/queue.module:246
msgid "This node has not yet been moderated."
msgstr "Цей вузол ще не модерувався."

#: modules/queue.module:283
msgid "The post is queued for approval. You can check the votes in the <a href=\"%queue\">submission queue</a>."
msgstr "Допис знаходиться у черзі на модерацію. Ви можете перевірити голоси у <a href=\"%queue\">черзі подання</a>."

#: modules/queue.module:286
msgid "The post is queued for approval. The editors will decide whether it should be published."
msgstr "Допис знаходиться у черзі на модерацію. Редактори вирішать, чи допис буде оприлюднено."

#: modules/queue.module:37
msgid "access submission queue"
msgstr "доступ до черги подання"

#: modules/queue.module:0
msgid "queue"
msgstr "черга"

