------------------------------------------------------------------------------
README.txt
-------------------------------------------------------------------------------

This is the Ukrainian translation of Drupal.

List of translated modules so far

/aggregator-module.po
/archive-module.po
/blog-module.po
/common-inc.po
/general.po
/menu-module.po
/poll-module.po
/search-module.po
/throttle-module.po
/watchdog-module.po

/taxonomy-module.po
/story-module.po
/forum-module.po
/filter-module.po
/file-inc.po
/drupal-module.po
/comment-module.po
/book-module.po

/block-module.po 
/blogapi-module.po 
/node-module.po    
/path-module.po    
/ping-module.po   
/profile-module.po   
/queue-module.po       
/statistics-module.po  
/system-module.po      
/upload-module.po      


------------------------
Update sep 11 2006
Updated all files for 4.6 version

-------------------------------------------------------------------------------
Translation by Maidan volunteer team

Translation project homepage in Ukrainian
http://alliance.maidanua.org/node/292
